namespace Base2art.Web.App.ObjectQualityManagement
{
    using System;
    using System.Linq;

    public static class ValidationHelper
    {
        public static Type ImplementOf(this Type type, Type interfaceType)
        {
            return type.GetInterfaces().FirstOrDefault(i => i.IsGenericType && i.GetGenericTypeDefinition() == interfaceType);
        }
    }
}