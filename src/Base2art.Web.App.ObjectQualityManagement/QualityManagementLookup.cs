namespace Base2art.Web.App.ObjectQualityManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Configuration;
    using Web.ObjectQualityManagement;
    using WebApiRunner.Server;

    public class QualityManagementLookup : IQualityManagementLookup
    {
        private readonly IAssemblyLoader loader;
        private readonly ICreator provider;

        private readonly Lazy<IReadOnlyDictionary<Type, IEnumerable<object>>> validators;
        private readonly Lazy<IReadOnlyDictionary<Type, IEnumerable<object>>> verifiers;

        public QualityManagementLookup(ICreator provider, IAssemblyLoader loader)
        {
            this.provider = provider;
            this.loader = loader;
            this.validators = new Lazy<IReadOnlyDictionary<Type, IEnumerable<object>>>(() => this.Create(typeof(IValidator<>)));
            this.verifiers = new Lazy<IReadOnlyDictionary<Type, IEnumerable<object>>>(() => this.Create(typeof(IVerifier<>)));
        }

        public IEnumerable<IValidator<T>> GetValidators<T>() => GetItems<T, IValidator<T>>(this.validators);

        public IEnumerable<IVerifier<T>> GetVerifiers<T>() => GetItems<T, IVerifier<T>>(this.verifiers);

        private static IEnumerable<T1> GetItems<T, T1>(Lazy<IReadOnlyDictionary<Type, IEnumerable<object>>> lazy)
        {
            var listOf = lazy.Value;

            var key = typeof(T);
            if (listOf.ContainsKey(key))
            {
                return listOf[key].Select(x => (T1) x);
            }

            return new T1[0];
        }

        private IReadOnlyDictionary<Type, IEnumerable<object>> Create(Type interfaceType)
        {
            var items = new Dictionary<Type, List<object>>();

            foreach (var assembly in this.loader.Assemblies().Where(x => !x.IsDynamic))
            {
                foreach (var type in GetExportedTypes(assembly))
                {
                    var implementsValidator = type.ImplementOf(interfaceType);

                    this.AddItemsToCollection(implementsValidator, items, type);
                }
            }

            return items.ToDictionary(x => x.Key, x => x.Value.Select(y => y));
        }

        private static Type[] GetExportedTypes(Assembly assembly)
        {
            try
            {
                return assembly.GetExportedTypes();
            }
            catch (Exception )
            {
                return new Type[0];
            }
        }

        private void AddItemsToCollection(Type implements, Dictionary<Type, List<object>> collection, Type type)
        {
            if (implements != null)
            {
                var key = implements.GetGenericArguments()[0];

                if (!key.IsGenericParameter)
                {
                    if (!collection.ContainsKey(key))
                    {
                        collection.Add(key, new List<object>());
                    }

                    collection[key].Add(this.CreateInstance(type));
                }
            }
        }

        private object CreateInstance(Type type) => this.provider.Create(type, null, null);
    }
}