﻿namespace Base2art.Web.App.ObjectQualityManagement
{
    using System;
    using System.Net;
    using ComponentModel.Composition;
    using Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Server.Registration;
    using Web.ObjectQualityManagement;
    using WebApiRunner.Server;

    public class Module : RegistrationBase
    {
        public override void RegisterSystemServicesPostUserService(IMvcCoreBuilder builder, IBindableServiceLoaderInjector services,
                                                                   IServerConfiguration config)
        {
            base.RegisterSystemServicesPostUserService(builder, services, config);
            services.Bind<IQualityManagementLookup>()
                    .To(new QualityManagementLookup(services.Resolve<ICreator>(), config.AssemblyLoader));
        }

        public override void RegisterFilters(IMvcCoreBuilder builder, IServiceProvider services, IServerConfiguration config)
        {
            base.RegisterFilters(builder, services, config);
            builder.AddMvcOptions(x => x.Filters.Add(new ValidationExceptionFilter<ValidationQualityManagementException>(
                                                                                                                         422,
                                                                                                                         "VALIDATION_FAILURE")));
            builder.AddMvcOptions(x => x.Filters.Add(new ValidationExceptionFilter<VerificationQualityManagementException>(
                                                                                                                           HttpStatusCode.Conflict,
                                                                                                                           "VALIDATION_FAILURE")));
        }
    }
}