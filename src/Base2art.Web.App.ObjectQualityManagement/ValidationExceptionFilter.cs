namespace Base2art.Web.App.ObjectQualityManagement
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Filters;
    using Microsoft.AspNetCore.Mvc.Filters;
    using Web.ObjectQualityManagement.Models;
    using WebApiRunner.Server;
    using WebApiRunner.Server.Filters;
    using WebApiRunner.Server.Filters.Models;

    public class ValidationExceptionFilter<T> : IAsyncExceptionFilter, IExceptionFilter
        where T : Base2art.Web.ObjectQualityManagement.QualityManagementException
    {
        private readonly int statusCode;
        private readonly string codeName;

        public ValidationExceptionFilter(int statusCode, string codeName)
        {
            this.statusCode = statusCode;
            this.codeName = codeName;
        }

        public ValidationExceptionFilter(HttpStatusCode statusCode, string codeName) : this((int) statusCode, codeName)
        {
        }

        /// <inheritdoc />
        public virtual Task OnExceptionAsync(ExceptionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            this.OnException(context);
            return Task.CompletedTask;
        }

        public virtual void OnException(ExceptionContext context)
        {
            if (context.Exception is T ex)
            {
                context.Exception.SuppressLogging();

                var response = context.HttpContext.Request.CreateResponse((HttpStatusCode) this.statusCode,
                                                                          new Error
                                                                          {
                                                                              Message = ex.Message,
                                                                              Code = this.codeName,
                                                                              Fields = ex?.Errors?.Select(x => Map(x))?.ToArray()
                                                                                       ?? new Base2art.Web.Exceptions.Models.ErrorField[0]
                                                                          });
                context.Result = response;

                context.Response().StatusCode = this.statusCode;
            }
        }

        private static Base2art.Web.Exceptions.Models.ErrorField Map(Base2art.Web.ObjectQualityManagement.Models.ErrorField x)
            => new Base2art.Web.Exceptions.Models.ErrorField {Code = x.Code, Message = x.Message, Path = x.Path};
    }
}