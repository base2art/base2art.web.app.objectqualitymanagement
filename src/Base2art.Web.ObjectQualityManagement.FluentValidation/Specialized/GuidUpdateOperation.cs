namespace Base2art.Web.ObjectQualityManagement.Specialized
{
    using System;

    public class GuidUpdateOperation : UpdateOperationBase<Guid>
    {
        public GuidUpdateOperation(Guid id, object data, Func<object, Guid> idLookup) : base(id, data, idLookup)
        {
        }
    }
}