namespace Base2art.Web.ObjectQualityManagement.Specialized
{
    using System;
    using System.Threading.Tasks;

    public static class ConventionalQualityManagers
    {
        public static async Task ValidateAndVerifyForUpdate<T>(this IQualityManagementLookup qa, Guid id, T data, Func<T, Guid> lookup)
        {
            var operation = new GuidUpdateOperation(id, data, (x) => lookup((T) x));
            await qa.ValidateAndVerify(operation);
            await qa.ValidateAndVerify(data);
        }
    }
}