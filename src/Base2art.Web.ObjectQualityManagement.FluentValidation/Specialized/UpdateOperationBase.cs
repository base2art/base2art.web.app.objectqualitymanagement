namespace Base2art.Web.ObjectQualityManagement.Specialized
{
    using System;

    public class UpdateOperationBase<T>
    {
        public UpdateOperationBase(T id, object data, Func<object, T> idLookup)
        {
            this.Id = id;
            this.Data = data;
            this.IdLookup = idLookup;
        }

        public T Id { get; }
        public object Data { get; }
        public Func<object, T> IdLookup { get; }
    }
}