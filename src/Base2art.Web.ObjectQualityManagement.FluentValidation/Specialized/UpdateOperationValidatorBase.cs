namespace Base2art.Web.ObjectQualityManagement.Specialized
{
    using FluentValidation;

    public class UpdateOperationValidatorBase<TId, TValidator> : ValidatorBase<TValidator>
        where TValidator : UpdateOperationBase<TId>
    {
        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Id).NotEmpty();
            this.Backing.RuleFor(x => x.Id).Equal((x) => x.IdLookup(x.Data));
            this.Backing.RuleFor(x => x.Data).NotNull();
        }
    }
}