namespace Base2art.Web.ObjectQualityManagement.Specialized
{
    using System;

    public class IntUpdateOperation : UpdateOperationBase<int>
    {
        public IntUpdateOperation(int id, object data, Func<object, int> idLookup) : base(id, data, idLookup)
        {
        }
    }
}