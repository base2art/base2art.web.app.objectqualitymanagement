namespace Base2art.Web.ObjectQualityManagement
{
    using System.Collections.Generic;
    using System.Linq;
    using FluentValidation;
    using FluentValidation.Results;

    public abstract class ValidatorBase<T> : IValidator<T>
    {
        private bool hasConfigured;

        public ValidatorBase() => this.Backing = new ConcreteValidator<T>();

        protected AbstractValidator<T> Backing { get; }

        public IQualityManagementResult Validate(T data)
        {
            if (!this.hasConfigured)
            {
                this.Configure();
                this.hasConfigured = true;
            }

            return this.Map(this.Backing.Validate(data));
        }

        protected abstract void Configure();

        private IQualityManagementResult Map(ValidationResult validate) =>
            new FakeValidationResult
            {
                Errors = (validate?.Errors ?? new List<ValidationFailure>()).Select(this.Map).ToArray(),
                IsValid = (validate?.IsValid).GetValueOrDefault(true)
            };

        private IQualityManagementError Map(ValidationFailure validate) =>
            new FakeValidationError
            {
                ErrorCode = validate?.ErrorCode,
                ErrorMessage = validate?.ErrorMessage,
                PropertyName = validate?.PropertyName
            };

        private class FakeValidationResult : IQualityManagementResult
        {
            public bool IsValid { get; set; }
            public IQualityManagementError[] Errors { get; set; } = new IQualityManagementError[0];
        }

        private class FakeValidationError : IQualityManagementError
        {
            public string ErrorCode { get; set; }
            public string ErrorMessage { get; set; }
            public string PropertyName { get; set; }
        }
    }
}