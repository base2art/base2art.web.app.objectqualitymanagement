namespace Base2art.Web.App.ObjectQualityManagement.Features
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ComponentModel.Composition;
    using Configuration;
    using Fixtures;
    using FluentAssertions;
    using Utils;
    using Web.ObjectQualityManagement;
    using WebApiRunner.Server;
    using Xunit;

    public class RealWorldSimulationFeature
    {
        [Fact]
        public async void Test1()
        {
            var loader = ServiceLoader.CreateLoader();
            var concreteValue = new PersonRepository();

            loader.Bind<IPersonRepository>().To(concreteValue);
            loader.Bind<ICreator>().To(x => new Creator(x));
            loader.Bind<IAssemblyLoader>().ToInstanceCreator<TestAssemblyFactory>();

            loader.Register(typeof(IQualityManagementLookup),
                            typeof(QualityManagementLookup),
                            new Dictionary<string, object>(),
                            new Dictionary<string, object>(),
                            true);

            loader.Register(typeof(PersonService),
                            typeof(PersonService),
                            new Dictionary<string, object>(),
                            new Dictionary<string, object>(),
                            false);

            loader.Seal();

            var personService = loader.Resolve<PersonService>();

            personService.Should().NotBeNull();

            var person = await personService.Add(new Person {Name = "SjY", SocialSecurityNumber = "123-34-5466"});

            new Func<Task>(() => personService.Add(new Person {Name = "MjY", SocialSecurityNumber = "123-34-5466"}))
                .Should().Throw<VerificationQualityManagementException>();

            new Func<Task>(() => personService.Update(person.Id, person))
                .Should().NotThrow();
            
            new Func<Task>(() => personService.Update(Guid.Empty, person))
                .Should().Throw<ValidationQualityManagementException>();
            
            new Func<Task>(() => personService.Update(Guid.NewGuid(), person))
                .Should().Throw<ValidationQualityManagementException>();

            person.SocialSecurityNumber = "2423-45-756";
            new Func<Task>(() => personService.Update(person.Id, person))
                .Should().Throw<ValidationQualityManagementException>();
        }
    }
}