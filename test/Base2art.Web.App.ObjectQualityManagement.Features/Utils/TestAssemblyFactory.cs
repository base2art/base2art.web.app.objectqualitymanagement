namespace Base2art.Web.App.ObjectQualityManagement.Features.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Configuration;

    public class TestAssemblyFactory : IAssemblyLoader
    {
        public IEnumerable<Assembly> Assemblies() => AppDomain.CurrentDomain.GetAssemblies();
    }
}