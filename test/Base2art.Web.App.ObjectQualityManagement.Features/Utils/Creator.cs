namespace Base2art.Web.App.ObjectQualityManagement.Features.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using ComponentModel.Composition;
    using WebApiRunner.Server;

    public class Creator : ICreator
    {
        private readonly IServiceLoaderInjector injector;

        public Creator(IServiceLoaderInjector injector) => this.injector = injector;

        public object Create(Type type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties)
        {
            var item = type.CreateFrom(this.injector, parameters, properties);
            return item;
        }

        public object Create(TypeInfo type, IReadOnlyDictionary<string, object> parameters, IReadOnlyDictionary<string, object> properties)
        {
            var item = type.CreateFrom(this.injector, parameters, properties);
            return item;
        }

        public object CreateAndInvoke(
            Type type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties)
            => throw new NotImplementedException();

        public object CreateAndInvoke(
            TypeInfo type,
            MethodInfo methodInfo,
            IReadOnlyDictionary<string, object> parameters,
            IReadOnlyDictionary<string, object> properties)
            => throw new NotImplementedException();
    }
}