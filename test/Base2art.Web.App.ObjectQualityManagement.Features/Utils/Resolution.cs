namespace Base2art.Web.App.ObjectQualityManagement.Features.Utils
{
    using System;
    using System.Collections.Generic;
    using ComponentModel.Composition;
    using Reflection.Activation;

    internal static class Resolution
    {
        public static T CreateFrom<T>(
            this IServiceLoaderInjector config,
            IReadOnlyDictionary<string, object> inputParms,
            IReadOnlyDictionary<string, object> properties)
            => new Wrapper<CircularDependencyException>(config).CreateFrom<T>(
                                                                              Wrap(inputParms),
                                                                              Wrap(properties));

        public static object CreateFrom(
            this Type type,
            IServiceLoaderInjector config,
            IReadOnlyDictionary<string, object> inputParms,
            IReadOnlyDictionary<string, object> properties)
            => type.CreateFrom(
                               new Wrapper<CircularDependencyException>(config),
                               Wrap(inputParms),
                               Wrap(properties));

        private static IReadOnlyDictionary<string, object> Wrap(IReadOnlyDictionary<string, object> dictionary)
            => dictionary ?? new Dictionary<string, object>();

        internal class Wrapper<T> : IServiceLookup
        {
            private readonly IServiceLoaderInjector config;

            public Wrapper(IServiceLoaderInjector config) => this.config = config;

            public object[] ResolveAll(Type contractType) => this.config.ResolveAll(contractType, true);

            public object Resolve(Type contractType, bool returnDefaultOnNotFound) => this.config.Resolve(contractType, returnDefaultOnNotFound);

            public Type PrimaryException => typeof(CircularDependencyException);

            public Exception Rethrow(string errorMessage, Exception inner) =>
                new CircularDependencyException(errorMessage, inner);
        }
    }
}