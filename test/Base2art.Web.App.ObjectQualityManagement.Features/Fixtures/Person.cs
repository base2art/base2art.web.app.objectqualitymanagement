namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System;

    public class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string SocialSecurityNumber { get; set; }
    }
}