namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using FluentValidation;
    using Utils;
    using Web.ObjectQualityManagement;

    public class PersonVerifier : VerifierBase<Person>
    {
        private readonly IPersonRepository ps;

        public PersonVerifier(IPersonRepository repository) => this.ps = repository;

        protected override void Configure()
        {
            var z = this.Backing.RuleFor(x => x.SocialSecurityNumber)
                        .MustAsync(async (p, ssn, cancellation) =>
                        {
                            var bySsn = await this.ps.GetBySSN(ssn);
                            return bySsn == null || bySsn.Id == p.Id;
                        })
                        .WithErrorCode("UniquenessError")
                        .WithMessage("'{PropertyName}' is already in use.");
        }
    }
}