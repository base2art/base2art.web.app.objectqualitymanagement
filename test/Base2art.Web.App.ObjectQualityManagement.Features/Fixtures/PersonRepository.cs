namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class PersonRepository : IPersonRepository
    {
        private readonly List<Person> people = new List<Person>();

        public Task<Guid> Add(Person person)
        {
            person.Id = Guid.NewGuid();
            this.people.Add(person);
            return Task.FromResult(person.Id);
        }

        public async Task<IEnumerable<Person>> All() => this.people.ToArray();
        public async Task<Person> GetBySSN(string ssn) => this.people.FirstOrDefault(x => x.SocialSecurityNumber == ssn);
    }
}