namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using FluentValidation;
    using Web.ObjectQualityManagement;

    public class CompanyValidator : ValidatorBase<Company>
    {
        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Name).NotEmpty();
        }
    }
}