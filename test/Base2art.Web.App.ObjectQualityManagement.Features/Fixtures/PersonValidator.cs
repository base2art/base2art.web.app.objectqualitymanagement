namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System.Text.RegularExpressions;
    using FluentValidation;
    using Web.ObjectQualityManagement;

    public class PersonValidator : ValidatorBase<Person>
    {
        protected override void Configure()
        {
            this.Backing.RuleFor(x => x.Name).NotEmpty();
            this.Backing.RuleFor(x => x.SocialSecurityNumber).Matches(new Regex(@"^\d{3}-\d{2}-\d{4}$"));
        }
    }
}