namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Web.ObjectQualityManagement;
    using Web.ObjectQualityManagement.Specialized;

    public class PersonService
    {
        private readonly IQualityManagementLookup qa;
        private readonly IPersonRepository repo;

        public PersonService(IQualityManagementLookup qa, IPersonRepository repo)
        {
            this.qa = qa;
            this.repo = repo;
        }

        public async Task<Person> Add(Person person)
        {
            await this.qa.ValidateAndVerify(person);
            await this.repo.Add(person);
            return person;
        }

        public async Task Update(Guid id, Person person)
        {
            await this.qa.ValidateAndVerifyForUpdate(id, person, x => x.Id);
            await this.repo.Add(person);
        }

        public Task<IEnumerable<Person>> GetAll() => this.repo.All();
    }
}