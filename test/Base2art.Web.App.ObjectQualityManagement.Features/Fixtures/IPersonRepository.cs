namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IPersonRepository
    {
        Task<Guid> Add(Person person);

        Task<IEnumerable<Person>> All();

        Task<Person> GetBySSN(string ssn);
    }
}