namespace Base2art.Web.App.ObjectQualityManagement.Features.Fixtures
{
    using System.Threading.Tasks;
    using Web.ObjectQualityManagement;

    public class CompanyVerifier : IVerifier<Company>
    {
        public Task<IQualityManagementResult> Verify(Company data) => null;
    }
}