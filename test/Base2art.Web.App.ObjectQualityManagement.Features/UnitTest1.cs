namespace Base2art.Web.App.ObjectQualityManagement.Features
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using ComponentModel.Composition;
    using Fixtures;
    using FluentAssertions;
    using Utils;
    using Web.ObjectQualityManagement;
    using Xunit;

    public class UnitTest1
    {
        [Fact]
        public async void Test1()
        {
//            ValidatorOptions.LanguageManager = new CustomLanguageManager();
            var loader = ServiceLoader.CreateLoader();
            var concreteValue = new PersonRepository();

            loader.Bind<IPersonRepository>().To(concreteValue);
            loader.Seal();
            var validationLookup = new QualityManagementLookup(new Creator(loader), new TestAssemblyFactory());

            var peopleValidators = validationLookup.GetValidators<Person>();
            var peopleVerifiers = validationLookup.GetVerifiers<Person>();

            peopleValidators.Count().Should().Be(1);
            peopleValidators.First().GetType().Should().Be<PersonValidator>();

            peopleVerifiers.Count().Should().Be(1);
            peopleVerifiers.First().GetType().Should().Be<PersonVerifier>();

            new Func<Task>(() => validationLookup.ValidateAndVerify(new Person())).Should().Throw<ValidationQualityManagementException>();
            var result1 = new Func<Task>(() => validationLookup.ValidateAndVerify(new Person
                                                                                  {
                                                                                      Name = "SjY",
                                                                                      SocialSecurityNumber = ""
                                                                                  })).Should().Throw<ValidationQualityManagementException>();
            result1.Subject.First().Errors.First().Code.Should().Be("RegularExpressionValidator");
            result1.Subject.First().Errors.First().Message.Should().Be("'Social Security Number' is not in the correct format.");
            result1.Subject.First().Errors.First().Path.Should().Be("SocialSecurityNumber");

            new Func<Task>(() => validationLookup.ValidateAndVerify(new Person
                                                                    {
                                                                        Id = Guid.NewGuid(),
                                                                        Name = "SjY",
                                                                        SocialSecurityNumber = "001-34-3453"
                                                                    })).Should().NotThrow();

            new Func<Task>(() => validationLookup.ValidateAndVerify(new Person
                                                                    {
                                                                        Id = Guid.NewGuid(),
                                                                        Name = "SjY ",
                                                                        SocialSecurityNumber = "123-45-66477"
                                                                    })).Should().Throw<ValidationQualityManagementException>();

            concreteValue.Add(new Person
                              {
                                  Name = "SjY",
                                  SocialSecurityNumber = "123-45-6647"
                              });

            var result = new Func<Task>(() => validationLookup.ValidateAndVerify(new Person
                                                                                 {
                                                                                     Id = Guid.NewGuid(),
                                                                                     Name = "Other",
                                                                                     SocialSecurityNumber = "123-45-6647"
                                                                                 })).Should().Throw<VerificationQualityManagementException>();

            result.Subject.First().Errors.First().Code.Should().Be("UniquenessError");
            result.Subject.First().Errors.First().Message.Should().Be("'Social Security Number' is already in use.");
            result.Subject.First().Errors.First().Path.Should().Be("SocialSecurityNumber");
        }
    }
}